## Simple HTTP Server

This is a simple HTTP server using Hapi instead of Express.  This is a project to create a simple server for dev purposes. Features and enchancements will be added as needed.

This server is intended to function similar to PHP 5's built in server (i.e. `php -S localhost:8000`).

The server currently can be ran by issuing `node server.js`.

## Features

* Hapi
* A server in a single file - server.js
* Basic routes for accessing /public file directory structure
* Used as a foundation for testing simple html pages or instances where a full server is not required.

 